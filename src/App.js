import React from "react";
import Tabs from "./components/Tabs";
import Information from "./components/Information";
import "./App.css";
import Survey from "./components/Survey";
import ChangePassword from "./components/ChangePassword";
import User from "./components/User";

function App() {
  const array = [
    { label: "Information", content: <Information /> },
    { label: "Survey", content: <Survey /> },
    { label: "Change Password", content: <ChangePassword /> },
    { label: "Users", content: <User /> },
  ];
  return (
    <div>
      <Tabs list={array}>
        {array.map((item, index) => {
          return (
            <div key={index} className="obj-content" style={{ height: `${100/array.length}%` }}>
              {item.content}
            </div>
          );
        })}
      </Tabs>
    </div>
  );
}

export default App;
