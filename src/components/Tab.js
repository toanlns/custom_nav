import React, { Component } from "react";

class Tab extends Component {
  
  onClick = () => {
    const { index, onClick } = this.props;
    onClick(index);
  };

  render() {
    const {
      onClick,
      props: { activeTab, label, index },
    } = this;

    let className = "tab-list-item";

    if (activeTab === index) {
      className += " tab-list-active";
    }

    return (
      <li className={className} onClick={onClick}>
        {label}
      </li>
    );
  }
}

export default Tab;
