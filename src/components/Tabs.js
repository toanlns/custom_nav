import React, { Component } from "react";
import Tab from "./Tab";

class Tabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
    };
  }
  onClickTabItem = (index) => {
    this.setState({
      activeTab: index,
    });
    const contentEl = document.getElementById("content");
    if (index === 0) {
      contentEl.style.top = "0";
    }

    if (index === 1) {
      contentEl.style.top = "-100%";
    }

    if (index === 2) {
      contentEl.style.top = "-200%";
    }
    if (index === 3) {
      contentEl.style.top = "-300%";
    }
  };

  render() {
    const {
      onClickTabItem,
      props: { list },
      state: { activeTab },
    } = this;

    return (
      <div className="tabs">
        <ol className="tab-list">
          {list.map((item, index) => {
            return (
              <Tab
                key={index}
                activeTab={activeTab}
                label={item.label}
                onClick={() => onClickTabItem(index)}
                index={index}
              />
            );
          })}
        </ol>
        <div className="tab-content" style={{ height: "400px" }}>
          <div
            className="content"
            style={{ height: `${list.length * 100}%` }}
            id="content"
          >
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default Tabs;
